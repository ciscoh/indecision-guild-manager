## Interface: 11302
## Title: INDECISION GUILD MANAGER
## Author: CiscOH-Blaumeux
## Notes: Comprehensive Guild, DKP and raid management utility.
## Version: 0.0.1
## DefaultState: enabled
## SavedVariables: IGMdb
## OptionalDeps: Ace3, Player Database Classic
## X-Embeds: Ace3

embeds.xml
enUS.lua
Core.lua